# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-11-24 10:36+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: git-gui.sh:41 git-gui.sh:604 git-gui.sh:618 git-gui.sh:631 git-gui.sh:714
#: git-gui.sh:733
msgid "git-gui: fatal error"
msgstr ""

#: git-gui.sh:565
#, tcl-format
msgid "Invalid font specified in %s:"
msgstr ""

#: git-gui.sh:590
msgid "Main Font"
msgstr ""

#: git-gui.sh:591
msgid "Diff/Console Font"
msgstr ""

#: git-gui.sh:605
msgid "Cannot find git in PATH."
msgstr ""

#: git-gui.sh:632
msgid "Cannot parse Git version string:"
msgstr ""

#: git-gui.sh:650
#, tcl-format
msgid ""
"Git version cannot be determined.\n"
"\n"
"%s claims it is version '%s'.\n"
"\n"
"%s requires at least Git 1.5.0 or later.\n"
"\n"
"Assume '%s' is version 1.5.0?\n"
msgstr ""

#: git-gui.sh:888
msgid "Git directory not found:"
msgstr ""

#: git-gui.sh:895
msgid "Cannot move to top of working directory:"
msgstr ""

#: git-gui.sh:902
msgid "Cannot use funny .git directory:"
msgstr ""

#: git-gui.sh:907
msgid "No working directory"
msgstr ""

#: git-gui.sh:1054
msgid "Refreshing file status..."
msgstr ""

#: git-gui.sh:1119
msgid "Scanning for modified files ..."
msgstr ""

#: git-gui.sh:1294 lib/browser.tcl:245
msgid "Ready."
msgstr ""

#: git-gui.sh:1560
msgid "Unmodified"
msgstr ""

#: git-gui.sh:1562
msgid "Modified, not staged"
msgstr ""

#: git-gui.sh:1563 git-gui.sh:1568
msgid "Staged for commit"
msgstr ""

#: git-gui.sh:1564 git-gui.sh:1569
msgid "Portions staged for commit"
msgstr ""

#: git-gui.sh:1565 git-gui.sh:1570
msgid "Staged for commit, missing"
msgstr ""

#: git-gui.sh:1567
msgid "Untracked, not staged"
msgstr ""

#: git-gui.sh:1572
msgid "Missing"
msgstr ""

#: git-gui.sh:1573
msgid "Staged for removal"
msgstr ""

#: git-gui.sh:1574
msgid "Staged for removal, still present"
msgstr ""

#: git-gui.sh:1576 git-gui.sh:1577 git-gui.sh:1578 git-gui.sh:1579
msgid "Requires merge resolution"
msgstr ""

#: git-gui.sh:1614
msgid "Starting gitk... please wait..."
msgstr ""

#: git-gui.sh:1623
#, tcl-format
msgid ""
"Unable to start gitk:\n"
"\n"
"%s does not exist"
msgstr ""

#: git-gui.sh:1823 lib/choose_repository.tcl:35
msgid "Repository"
msgstr ""

#: git-gui.sh:1824
msgid "Edit"
msgstr ""

#: git-gui.sh:1826 lib/choose_rev.tcl:560
msgid "Branch"
msgstr ""

#: git-gui.sh:1829 lib/choose_rev.tcl:547
msgid "Commit@@noun"
msgstr ""

#: git-gui.sh:1832 lib/merge.tcl:121 lib/merge.tcl:150 lib/merge.tcl:168
msgid "Merge"
msgstr ""

#: git-gui.sh:1833 lib/choose_rev.tcl:556
msgid "Remote"
msgstr ""

#: git-gui.sh:1842
msgid "Browse Current Branch's Files"
msgstr ""

#: git-gui.sh:1846
msgid "Browse Branch Files..."
msgstr ""

#: git-gui.sh:1851
msgid "Visualize Current Branch's History"
msgstr ""

#: git-gui.sh:1855
msgid "Visualize All Branch History"
msgstr ""

#: git-gui.sh:1862
#, tcl-format
msgid "Browse %s's Files"
msgstr ""

#: git-gui.sh:1864
#, tcl-format
msgid "Visualize %s's History"
msgstr ""

#: git-gui.sh:1869 lib/database.tcl:27 lib/database.tcl:67
msgid "Database Statistics"
msgstr ""

#: git-gui.sh:1872 lib/database.tcl:34
msgid "Compress Database"
msgstr ""

#: git-gui.sh:1875
msgid "Verify Database"
msgstr ""

#: git-gui.sh:1882 git-gui.sh:1886 git-gui.sh:1890 lib/shortcut.tcl:7
#: lib/shortcut.tcl:39 lib/shortcut.tcl:71
msgid "Create Desktop Icon"
msgstr ""

#: git-gui.sh:1895 lib/choose_repository.tcl:176 lib/choose_repository.tcl:184
msgid "Quit"
msgstr ""

#: git-gui.sh:1902
msgid "Undo"
msgstr ""

#: git-gui.sh:1905
msgid "Redo"
msgstr ""

#: git-gui.sh:1909 git-gui.sh:2403
msgid "Cut"
msgstr ""

#: git-gui.sh:1912 git-gui.sh:2406 git-gui.sh:2477 git-gui.sh:2549
#: lib/console.tcl:67
msgid "Copy"
msgstr ""

#: git-gui.sh:1915 git-gui.sh:2409
msgid "Paste"
msgstr ""

#: git-gui.sh:1918 git-gui.sh:2412 lib/branch_delete.tcl:26
#: lib/remote_branch_delete.tcl:38
msgid "Delete"
msgstr ""

#: git-gui.sh:1922 git-gui.sh:2416 git-gui.sh:2553 lib/console.tcl:69
msgid "Select All"
msgstr ""

#: git-gui.sh:1931
msgid "Create..."
msgstr ""

#: git-gui.sh:1937
msgid "Checkout..."
msgstr ""

#: git-gui.sh:1943
msgid "Rename..."
msgstr ""

#: git-gui.sh:1948 git-gui.sh:2048
msgid "Delete..."
msgstr ""

#: git-gui.sh:1953
msgid "Reset..."
msgstr ""

#: git-gui.sh:1965 git-gui.sh:2350
msgid "New Commit"
msgstr ""

#: git-gui.sh:1973 git-gui.sh:2357
msgid "Amend Last Commit"
msgstr ""

#: git-gui.sh:1982 git-gui.sh:2317 lib/remote_branch_delete.tcl:99
msgid "Rescan"
msgstr ""

#: git-gui.sh:1988
msgid "Stage To Commit"
msgstr ""

#: git-gui.sh:1994
msgid "Stage Changed Files To Commit"
msgstr ""

#: git-gui.sh:2000
msgid "Unstage From Commit"
msgstr ""

#: git-gui.sh:2005 lib/index.tcl:393
msgid "Revert Changes"
msgstr ""

#: git-gui.sh:2012 git-gui.sh:2329 git-gui.sh:2427
msgid "Sign Off"
msgstr ""

#: git-gui.sh:2016 git-gui.sh:2333
msgid "Commit@@verb"
msgstr ""

#: git-gui.sh:2027
msgid "Local Merge..."
msgstr ""

#: git-gui.sh:2032
msgid "Abort Merge..."
msgstr ""

#: git-gui.sh:2044
msgid "Push..."
msgstr ""

#: git-gui.sh:2055 lib/choose_repository.tcl:40
msgid "Apple"
msgstr ""

#: git-gui.sh:2058 git-gui.sh:2080 lib/about.tcl:13
#: lib/choose_repository.tcl:43 lib/choose_repository.tcl:49
#, tcl-format
msgid "About %s"
msgstr ""

#: git-gui.sh:2062
msgid "Preferences..."
msgstr ""

#: git-gui.sh:2070 git-gui.sh:2595
msgid "Options..."
msgstr ""

#: git-gui.sh:2076 lib/choose_repository.tcl:46
msgid "Help"
msgstr ""

#: git-gui.sh:2117
msgid "Online Documentation"
msgstr ""

#: git-gui.sh:2201
#, tcl-format
msgid "fatal: cannot stat path %s: No such file or directory"
msgstr ""

#: git-gui.sh:2234
msgid "Current Branch:"
msgstr ""

#: git-gui.sh:2255
msgid "Staged Changes (Will Commit)"
msgstr ""

#: git-gui.sh:2274
msgid "Unstaged Changes"
msgstr ""

#: git-gui.sh:2323
msgid "Stage Changed"
msgstr ""

#: git-gui.sh:2339 lib/transport.tcl:93 lib/transport.tcl:182
msgid "Push"
msgstr ""

#: git-gui.sh:2369
msgid "Initial Commit Message:"
msgstr ""

#: git-gui.sh:2370
msgid "Amended Commit Message:"
msgstr ""

#: git-gui.sh:2371
msgid "Amended Initial Commit Message:"
msgstr ""

#: git-gui.sh:2372
msgid "Amended Merge Commit Message:"
msgstr ""

#: git-gui.sh:2373
msgid "Merge Commit Message:"
msgstr ""

#: git-gui.sh:2374
msgid "Commit Message:"
msgstr ""

#: git-gui.sh:2419 git-gui.sh:2557 lib/console.tcl:71
msgid "Copy All"
msgstr ""

#: git-gui.sh:2443 lib/blame.tcl:104
msgid "File:"
msgstr ""

#: git-gui.sh:2545
msgid "Refresh"
msgstr ""

#: git-gui.sh:2566
msgid "Apply/Reverse Hunk"
msgstr ""

#: git-gui.sh:2572
msgid "Decrease Font Size"
msgstr ""

#: git-gui.sh:2576
msgid "Increase Font Size"
msgstr ""

#: git-gui.sh:2581
msgid "Show Less Context"
msgstr ""

#: git-gui.sh:2588
msgid "Show More Context"
msgstr ""

#: git-gui.sh:2602
msgid "Unstage Hunk From Commit"
msgstr ""

#: git-gui.sh:2604
msgid "Stage Hunk For Commit"
msgstr ""

#: git-gui.sh:2623
msgid "Initializing..."
msgstr ""

#: git-gui.sh:2718
#, tcl-format
msgid ""
"Possible environment issues exist.\n"
"\n"
"The following environment variables are probably\n"
"going to be ignored by any Git subprocess run\n"
"by %s:\n"
"\n"
msgstr ""

#: git-gui.sh:2748
msgid ""
"\n"
"This is due to a known issue with the\n"
"Tcl binary distributed by Cygwin."
msgstr ""

#: git-gui.sh:2753
#, tcl-format
msgid ""
"\n"
"\n"
"A good replacement for %s\n"
"is placing values for the user.name and\n"
"user.email settings into your personal\n"
"~/.gitconfig file.\n"
msgstr ""

#: lib/about.tcl:25
msgid "git-gui - a graphical user interface for Git."
msgstr ""

#: lib/blame.tcl:77
msgid "File Viewer"
msgstr ""

#: lib/blame.tcl:81
msgid "Commit:"
msgstr ""

#: lib/blame.tcl:249
msgid "Copy Commit"
msgstr ""

#: lib/blame.tcl:369
#, tcl-format
msgid "Reading %s..."
msgstr ""

#: lib/blame.tcl:473
msgid "Loading copy/move tracking annotations..."
msgstr ""

#: lib/blame.tcl:493
msgid "lines annotated"
msgstr ""

#: lib/blame.tcl:674
msgid "Loading original location annotations..."
msgstr ""

#: lib/blame.tcl:677
msgid "Annotation complete."
msgstr ""

#: lib/blame.tcl:731
msgid "Loading annotation..."
msgstr ""

#: lib/blame.tcl:787
msgid "Author:"
msgstr ""

#: lib/blame.tcl:791
msgid "Committer:"
msgstr ""

#: lib/blame.tcl:796
msgid "Original File:"
msgstr ""

#: lib/blame.tcl:910
msgid "Originally By:"
msgstr ""

#: lib/blame.tcl:916
msgid "In File:"
msgstr ""

#: lib/blame.tcl:921
msgid "Copied Or Moved Here By:"
msgstr ""

#: lib/branch_checkout.tcl:14 lib/branch_checkout.tcl:19
msgid "Checkout Branch"
msgstr ""

#: lib/branch_checkout.tcl:23
msgid "Checkout"
msgstr ""

#: lib/branch_checkout.tcl:27 lib/branch_create.tcl:35
#: lib/branch_delete.tcl:32 lib/branch_rename.tcl:30 lib/browser.tcl:281
#: lib/checkout_op.tcl:522 lib/choose_font.tcl:43 lib/merge.tcl:172
#: lib/option.tcl:90 lib/remote_branch_delete.tcl:42 lib/transport.tcl:97
msgid "Cancel"
msgstr ""

#: lib/branch_checkout.tcl:32 lib/browser.tcl:286
msgid "Revision"
msgstr ""

#: lib/branch_checkout.tcl:36 lib/branch_create.tcl:69 lib/option.tcl:202
msgid "Options"
msgstr ""

#: lib/branch_checkout.tcl:39 lib/branch_create.tcl:92
msgid "Fetch Tracking Branch"
msgstr ""

#: lib/branch_checkout.tcl:44
msgid "Detach From Local Branch"
msgstr ""

#: lib/branch_create.tcl:22
msgid "Create Branch"
msgstr ""

#: lib/branch_create.tcl:27
msgid "Create New Branch"
msgstr ""

#: lib/branch_create.tcl:31 lib/choose_repository.tcl:375
msgid "Create"
msgstr ""

#: lib/branch_create.tcl:40
msgid "Branch Name"
msgstr ""

#: lib/branch_create.tcl:43
msgid "Name:"
msgstr ""

#: lib/branch_create.tcl:58
msgid "Match Tracking Branch Name"
msgstr ""

#: lib/branch_create.tcl:66
msgid "Starting Revision"
msgstr ""

#: lib/branch_create.tcl:72
msgid "Update Existing Branch:"
msgstr ""

#: lib/branch_create.tcl:75
msgid "No"
msgstr ""

#: lib/branch_create.tcl:80
msgid "Fast Forward Only"
msgstr ""

#: lib/branch_create.tcl:85 lib/checkout_op.tcl:514
msgid "Reset"
msgstr ""

#: lib/branch_create.tcl:97
msgid "Checkout After Creation"
msgstr ""

#: lib/branch_create.tcl:131
msgid "Please select a tracking branch."
msgstr ""

#: lib/branch_create.tcl:140
#, tcl-format
msgid "Tracking branch %s is not a branch in the remote repository."
msgstr ""

#: lib/branch_create.tcl:153 lib/branch_rename.tcl:86
msgid "Please supply a branch name."
msgstr ""

#: lib/branch_create.tcl:164 lib/branch_rename.tcl:106
#, tcl-format
msgid "'%s' is not an acceptable branch name."
msgstr ""

#: lib/branch_delete.tcl:15
msgid "Delete Branch"
msgstr ""

#: lib/branch_delete.tcl:20
msgid "Delete Local Branch"
msgstr ""

#: lib/branch_delete.tcl:37
msgid "Local Branches"
msgstr ""

#: lib/branch_delete.tcl:52
msgid "Delete Only If Merged Into"
msgstr ""

#: lib/branch_delete.tcl:54
msgid "Always (Do not perform merge test.)"
msgstr ""

#: lib/branch_delete.tcl:103
#, tcl-format
msgid "The following branches are not completely merged into %s:"
msgstr ""

#: lib/branch_delete.tcl:115
msgid ""
"Recovering deleted branches is difficult. \n"
"\n"
" Delete the selected branches?"
msgstr ""

#: lib/branch_delete.tcl:141
#, tcl-format
msgid ""
"Failed to delete branches:\n"
"%s"
msgstr ""

#: lib/branch_rename.tcl:14 lib/branch_rename.tcl:22
msgid "Rename Branch"
msgstr ""

#: lib/branch_rename.tcl:26
msgid "Rename"
msgstr ""

#: lib/branch_rename.tcl:36
msgid "Branch:"
msgstr ""

#: lib/branch_rename.tcl:39
msgid "New Name:"
msgstr ""

#: lib/branch_rename.tcl:75
msgid "Please select a branch to rename."
msgstr ""

#: lib/branch_rename.tcl:96 lib/checkout_op.tcl:179
#, tcl-format
msgid "Branch '%s' already exists."
msgstr ""

#: lib/branch_rename.tcl:117
#, tcl-format
msgid "Failed to rename '%s'."
msgstr ""

#: lib/browser.tcl:17
msgid "Starting..."
msgstr ""

#: lib/browser.tcl:26
msgid "File Browser"
msgstr ""

#: lib/browser.tcl:125 lib/browser.tcl:142
#, tcl-format
msgid "Loading %s..."
msgstr ""

#: lib/browser.tcl:186
msgid "[Up To Parent]"
msgstr ""

#: lib/browser.tcl:266 lib/browser.tcl:272
msgid "Browse Branch Files"
msgstr ""

#: lib/browser.tcl:277 lib/choose_repository.tcl:391
#: lib/choose_repository.tcl:482 lib/choose_repository.tcl:492
#: lib/choose_repository.tcl:989
msgid "Browse"
msgstr ""

#: lib/checkout_op.tcl:79
#, tcl-format
msgid "Fetching %s from %s"
msgstr ""

#: lib/checkout_op.tcl:127
#, tcl-format
msgid "fatal: Cannot resolve %s"
msgstr ""

#: lib/checkout_op.tcl:140 lib/console.tcl:79 lib/database.tcl:31
msgid "Close"
msgstr ""

#: lib/checkout_op.tcl:169
#, tcl-format
msgid "Branch '%s' does not exist."
msgstr ""

#: lib/checkout_op.tcl:206
#, tcl-format
msgid ""
"Branch '%s' already exists.\n"
"\n"
"It cannot fast-forward to %s.\n"
"A merge is required."
msgstr ""

#: lib/checkout_op.tcl:220
#, tcl-format
msgid "Merge strategy '%s' not supported."
msgstr ""

#: lib/checkout_op.tcl:239
#, tcl-format
msgid "Failed to update '%s'."
msgstr ""

#: lib/checkout_op.tcl:251
msgid "Staging area (index) is already locked."
msgstr ""

#: lib/checkout_op.tcl:266
msgid ""
"Last scanned state does not match repository state.\n"
"\n"
"Another Git program has modified this repository since the last scan.  A "
"rescan must be performed before the current branch can be changed.\n"
"\n"
"The rescan will be automatically started now.\n"
msgstr ""

#: lib/checkout_op.tcl:322
#, tcl-format
msgid "Updating working directory to '%s'..."
msgstr ""

#: lib/checkout_op.tcl:353
#, tcl-format
msgid "Aborted checkout of '%s' (file level merging is required)."
msgstr ""

#: lib/checkout_op.tcl:354
msgid "File level merge required."
msgstr ""

#: lib/checkout_op.tcl:358
#, tcl-format
msgid "Staying on branch '%s'."
msgstr ""

#: lib/checkout_op.tcl:429
msgid ""
"You are no longer on a local branch.\n"
"\n"
"If you wanted to be on a branch, create one now starting from 'This Detached "
"Checkout'."
msgstr ""

#: lib/checkout_op.tcl:446
#, tcl-format
msgid "Checked out '%s'."
msgstr ""

#: lib/checkout_op.tcl:478
#, tcl-format
msgid "Resetting '%s' to '%s' will lose the following commits:"
msgstr ""

#: lib/checkout_op.tcl:500
msgid "Recovering lost commits may not be easy."
msgstr ""

#: lib/checkout_op.tcl:505
#, tcl-format
msgid "Reset '%s'?"
msgstr ""

#: lib/checkout_op.tcl:510 lib/merge.tcl:164
msgid "Visualize"
msgstr ""

#: lib/checkout_op.tcl:578
#, tcl-format
msgid ""
"Failed to set current branch.\n"
"\n"
"This working directory is only partially switched.  We successfully updated "
"your files, but failed to update an internal Git file.\n"
"\n"
"This should not have occurred.  %s will now close and give up."
msgstr ""

#: lib/choose_font.tcl:39
msgid "Select"
msgstr ""

#: lib/choose_font.tcl:53
msgid "Font Family"
msgstr ""

#: lib/choose_font.tcl:73
msgid "Font Size"
msgstr ""

#: lib/choose_font.tcl:90
msgid "Font Example"
msgstr ""

#: lib/choose_font.tcl:101
msgid ""
"This is example text.\n"
"If you like this text, it can be your font."
msgstr ""

#: lib/choose_repository.tcl:27
msgid "Git Gui"
msgstr ""

#: lib/choose_repository.tcl:80 lib/choose_repository.tcl:380
msgid "Create New Repository"
msgstr ""

#: lib/choose_repository.tcl:86
msgid "New..."
msgstr ""

#: lib/choose_repository.tcl:93 lib/choose_repository.tcl:468
msgid "Clone Existing Repository"
msgstr ""

#: lib/choose_repository.tcl:99
msgid "Clone..."
msgstr ""

#: lib/choose_repository.tcl:106 lib/choose_repository.tcl:978
msgid "Open Existing Repository"
msgstr ""

#: lib/choose_repository.tcl:112
msgid "Open..."
msgstr ""

#: lib/choose_repository.tcl:125
msgid "Recent Repositories"
msgstr ""

#: lib/choose_repository.tcl:131
msgid "Open Recent Repository:"
msgstr ""

#: lib/choose_repository.tcl:294
#, tcl-format
msgid "Location %s already exists."
msgstr ""

#: lib/choose_repository.tcl:300 lib/choose_repository.tcl:307
#: lib/choose_repository.tcl:314
#, tcl-format
msgid "Failed to create repository %s:"
msgstr ""

#: lib/choose_repository.tcl:385 lib/choose_repository.tcl:486
msgid "Directory:"
msgstr ""

#: lib/choose_repository.tcl:415 lib/choose_repository.tcl:544
#: lib/choose_repository.tcl:1013
msgid "Git Repository"
msgstr ""

#: lib/choose_repository.tcl:430 lib/choose_repository.tcl:437
#, tcl-format
msgid "Directory %s already exists."
msgstr ""

#: lib/choose_repository.tcl:442
#, tcl-format
msgid "File %s already exists."
msgstr ""

#: lib/choose_repository.tcl:463
msgid "Clone"
msgstr ""

#: lib/choose_repository.tcl:476
msgid "URL:"
msgstr ""

#: lib/choose_repository.tcl:496
msgid "Clone Type:"
msgstr ""

#: lib/choose_repository.tcl:502
msgid "Standard (Fast, Semi-Redundant, Hardlinks)"
msgstr ""

#: lib/choose_repository.tcl:508
msgid "Full Copy (Slower, Redundant Backup)"
msgstr ""

#: lib/choose_repository.tcl:514
msgid "Shared (Fastest, Not Recommended, No Backup)"
msgstr ""

#: lib/choose_repository.tcl:550 lib/choose_repository.tcl:597
#: lib/choose_repository.tcl:738 lib/choose_repository.tcl:808
#: lib/choose_repository.tcl:1019 lib/choose_repository.tcl:1027
#, tcl-format
msgid "Not a Git repository: %s"
msgstr ""

#: lib/choose_repository.tcl:586
msgid "Standard only available for local repository."
msgstr ""

#: lib/choose_repository.tcl:590
msgid "Shared only available for local repository."
msgstr ""

#: lib/choose_repository.tcl:617
msgid "Failed to configure origin"
msgstr ""

#: lib/choose_repository.tcl:629
msgid "Counting objects"
msgstr ""

#: lib/choose_repository.tcl:630
msgid "buckets"
msgstr ""

#: lib/choose_repository.tcl:654
#, tcl-format
msgid "Unable to copy objects/info/alternates: %s"
msgstr ""

#: lib/choose_repository.tcl:690
#, tcl-format
msgid "Nothing to clone from %s."
msgstr ""

#: lib/choose_repository.tcl:692 lib/choose_repository.tcl:906
#: lib/choose_repository.tcl:918
msgid "The 'master' branch has not been initialized."
msgstr ""

#: lib/choose_repository.tcl:705
msgid "Hardlinks are unavailable.  Falling back to copying."
msgstr ""

#: lib/choose_repository.tcl:717
#, tcl-format
msgid "Cloning from %s"
msgstr ""

#: lib/choose_repository.tcl:748
msgid "Copying objects"
msgstr ""

#: lib/choose_repository.tcl:749
msgid "KiB"
msgstr ""

#: lib/choose_repository.tcl:773
#, tcl-format
msgid "Unable to copy object: %s"
msgstr ""

#: lib/choose_repository.tcl:783
msgid "Linking objects"
msgstr ""

#: lib/choose_repository.tcl:784
msgid "objects"
msgstr ""

#: lib/choose_repository.tcl:792
#, tcl-format
msgid "Unable to hardlink object: %s"
msgstr ""

#: lib/choose_repository.tcl:847
msgid "Cannot fetch branches and objects.  See console output for details."
msgstr ""

#: lib/choose_repository.tcl:858
msgid "Cannot fetch tags.  See console output for details."
msgstr ""

#: lib/choose_repository.tcl:882
msgid "Cannot determine HEAD.  See console output for details."
msgstr ""

#: lib/choose_repository.tcl:891
#, tcl-format
msgid "Unable to cleanup %s"
msgstr ""

#: lib/choose_repository.tcl:897
msgid "Clone failed."
msgstr ""

#: lib/choose_repository.tcl:904
msgid "No default branch obtained."
msgstr ""

#: lib/choose_repository.tcl:915
#, tcl-format
msgid "Cannot resolve %s as a commit."
msgstr ""

#: lib/choose_repository.tcl:927
msgid "Creating working directory"
msgstr ""

#: lib/choose_repository.tcl:928 lib/index.tcl:65 lib/index.tcl:127
#: lib/index.tcl:193
msgid "files"
msgstr ""

#: lib/choose_repository.tcl:957
msgid "Initial file checkout failed."
msgstr ""

#: lib/choose_repository.tcl:973
msgid "Open"
msgstr ""

#: lib/choose_repository.tcl:983
msgid "Repository:"
msgstr ""

#: lib/choose_repository.tcl:1033
#, tcl-format
msgid "Failed to open repository %s:"
msgstr ""

#: lib/choose_rev.tcl:53
msgid "This Detached Checkout"
msgstr ""

#: lib/choose_rev.tcl:60
msgid "Revision Expression:"
msgstr ""

#: lib/choose_rev.tcl:74
msgid "Local Branch"
msgstr ""

#: lib/choose_rev.tcl:79
msgid "Tracking Branch"
msgstr ""

#: lib/choose_rev.tcl:84 lib/choose_rev.tcl:537
msgid "Tag"
msgstr ""

#: lib/choose_rev.tcl:317
#, tcl-format
msgid "Invalid revision: %s"
msgstr ""

#: lib/choose_rev.tcl:338
msgid "No revision selected."
msgstr ""

#: lib/choose_rev.tcl:346
msgid "Revision expression is empty."
msgstr ""

#: lib/choose_rev.tcl:530
msgid "Updated"
msgstr ""

#: lib/choose_rev.tcl:558
msgid "URL"
msgstr ""

#: lib/commit.tcl:9
msgid ""
"There is nothing to amend.\n"
"\n"
"You are about to create the initial commit.  There is no commit before this "
"to amend.\n"
msgstr ""

#: lib/commit.tcl:18
msgid ""
"Cannot amend while merging.\n"
"\n"
"You are currently in the middle of a merge that has not been fully "
"completed.  You cannot amend the prior commit unless you first abort the "
"current merge activity.\n"
msgstr ""

#: lib/commit.tcl:49
msgid "Error loading commit data for amend:"
msgstr ""

#: lib/commit.tcl:76
msgid "Unable to obtain your identity:"
msgstr ""

#: lib/commit.tcl:81
msgid "Invalid GIT_COMMITTER_IDENT:"
msgstr ""

#: lib/commit.tcl:133
msgid ""
"Last scanned state does not match repository state.\n"
"\n"
"Another Git program has modified this repository since the last scan.  A "
"rescan must be performed before another commit can be created.\n"
"\n"
"The rescan will be automatically started now.\n"
msgstr ""

#: lib/commit.tcl:154
#, tcl-format
msgid ""
"Unmerged files cannot be committed.\n"
"\n"
"File %s has merge conflicts.  You must resolve them and stage the file "
"before committing.\n"
msgstr ""

#: lib/commit.tcl:162
#, tcl-format
msgid ""
"Unknown file state %s detected.\n"
"\n"
"File %s cannot be committed by this program.\n"
msgstr ""

#: lib/commit.tcl:170
msgid ""
"No changes to commit.\n"
"\n"
"You must stage at least 1 file before you can commit.\n"
msgstr ""

#: lib/commit.tcl:183
msgid ""
"Please supply a commit message.\n"
"\n"
"A good commit message has the following format:\n"
"\n"
"- First line: Describe in one sentence what you did.\n"
"- Second line: Blank\n"
"- Remaining lines: Describe why this change is good.\n"
msgstr ""

#: lib/commit.tcl:257
msgid "write-tree failed:"
msgstr ""

#: lib/commit.tcl:275
#, tcl-format
msgid "Commit %s appears to be corrupt"
msgstr ""

#: lib/commit.tcl:279
msgid ""
"No changes to commit.\n"
"\n"
"No files were modified by this commit and it was not a merge commit.\n"
"\n"
"A rescan will be automatically started now.\n"
msgstr ""

#: lib/commit.tcl:286
msgid "No changes to commit."
msgstr ""

#: lib/commit.tcl:303
#, tcl-format
msgid "warning: Tcl does not support encoding '%s'."
msgstr ""

#: lib/commit.tcl:317
msgid "commit-tree failed:"
msgstr ""

#: lib/commit.tcl:339
msgid "update-ref failed:"
msgstr ""

#: lib/commit.tcl:430
#, tcl-format
msgid "Created commit %s: %s"
msgstr ""

#: lib/console.tcl:57
msgid "Working... please wait..."
msgstr ""

#: lib/console.tcl:183
msgid "Success"
msgstr ""

#: lib/console.tcl:196
msgid "Error: Command Failed"
msgstr ""

#: lib/database.tcl:43
msgid "Number of loose objects"
msgstr ""

#: lib/database.tcl:44
msgid "Disk space used by loose objects"
msgstr ""

#: lib/database.tcl:45
msgid "Number of packed objects"
msgstr ""

#: lib/database.tcl:46
msgid "Number of packs"
msgstr ""

#: lib/database.tcl:47
msgid "Disk space used by packed objects"
msgstr ""

#: lib/database.tcl:48
msgid "Packed objects waiting for pruning"
msgstr ""

#: lib/database.tcl:49
msgid "Garbage files"
msgstr ""

#: lib/database.tcl:72
msgid "Compressing the object database"
msgstr ""

#: lib/database.tcl:83
msgid "Verifying the object database with fsck-objects"
msgstr ""

#: lib/database.tcl:108
#, tcl-format
msgid ""
"This repository currently has approximately %i loose objects.\n"
"\n"
"To maintain optimal performance it is strongly recommended that you compress "
"the database when more than %i loose objects exist.\n"
"\n"
"Compress the database now?"
msgstr ""

#: lib/date.tcl:25
#, tcl-format
msgid "Invalid date from Git: %s"
msgstr ""

#: lib/diff.tcl:42
#, tcl-format
msgid ""
"No differences detected.\n"
"\n"
"%s has no changes.\n"
"\n"
"The modification date of this file was updated by another application, but "
"the content within the file was not changed.\n"
"\n"
"A rescan will be automatically started to find other files which may have "
"the same state."
msgstr ""

#: lib/diff.tcl:81
#, tcl-format
msgid "Loading diff of %s..."
msgstr ""

#: lib/diff.tcl:114 lib/diff.tcl:184
#, tcl-format
msgid "Unable to display %s"
msgstr ""

#: lib/diff.tcl:115
msgid "Error loading file:"
msgstr ""

#: lib/diff.tcl:122
msgid "Git Repository (subproject)"
msgstr ""

#: lib/diff.tcl:134
msgid "* Binary file (not showing content)."
msgstr ""

#: lib/diff.tcl:185
msgid "Error loading diff:"
msgstr ""

#: lib/diff.tcl:302
msgid "Failed to unstage selected hunk."
msgstr ""

#: lib/diff.tcl:309
msgid "Failed to stage selected hunk."
msgstr ""

#: lib/error.tcl:12 lib/error.tcl:102
msgid "error"
msgstr ""

#: lib/error.tcl:28
msgid "warning"
msgstr ""

#: lib/error.tcl:81
msgid "You must correct the above errors before committing."
msgstr ""

#: lib/index.tcl:6
msgid "Unable to unlock the index."
msgstr ""

#: lib/index.tcl:15
msgid "Index Error"
msgstr ""

#: lib/index.tcl:21
msgid ""
"Updating the Git index failed.  A rescan will be automatically started to "
"resynchronize git-gui."
msgstr ""

#: lib/index.tcl:27
msgid "Continue"
msgstr ""

#: lib/index.tcl:31
msgid "Unlock Index"
msgstr ""

#: lib/index.tcl:282
#, tcl-format
msgid "Unstaging %s from commit"
msgstr ""

#: lib/index.tcl:326
#, tcl-format
msgid "Adding %s"
msgstr ""

#: lib/index.tcl:381
#, tcl-format
msgid "Revert changes in file %s?"
msgstr ""

#: lib/index.tcl:383
#, tcl-format
msgid "Revert changes in these %i files?"
msgstr ""

#: lib/index.tcl:389
msgid "Any unstaged changes will be permanently lost by the revert."
msgstr ""

#: lib/index.tcl:392
msgid "Do Nothing"
msgstr ""

#: lib/merge.tcl:13
msgid ""
"Cannot merge while amending.\n"
"\n"
"You must finish amending this commit before starting any type of merge.\n"
msgstr ""

#: lib/merge.tcl:27
msgid ""
"Last scanned state does not match repository state.\n"
"\n"
"Another Git program has modified this repository since the last scan.  A "
"rescan must be performed before a merge can be performed.\n"
"\n"
"The rescan will be automatically started now.\n"
msgstr ""

#: lib/merge.tcl:44
#, tcl-format
msgid ""
"You are in the middle of a conflicted merge.\n"
"\n"
"File %s has merge conflicts.\n"
"\n"
"You must resolve them, stage the file, and commit to complete the current "
"merge.  Only then can you begin another merge.\n"
msgstr ""

#: lib/merge.tcl:54
#, tcl-format
msgid ""
"You are in the middle of a change.\n"
"\n"
"File %s is modified.\n"
"\n"
"You should complete the current commit before starting a merge.  Doing so "
"will help you abort a failed merge, should the need arise.\n"
msgstr ""

#: lib/merge.tcl:106
#, tcl-format
msgid "%s of %s"
msgstr ""

#: lib/merge.tcl:119
#, tcl-format
msgid "Merging %s and %s"
msgstr ""

#: lib/merge.tcl:131
msgid "Merge completed successfully."
msgstr ""

#: lib/merge.tcl:133
msgid "Merge failed.  Conflict resolution is required."
msgstr ""

#: lib/merge.tcl:158
#, tcl-format
msgid "Merge Into %s"
msgstr ""

#: lib/merge.tcl:177
msgid "Revision To Merge"
msgstr ""

#: lib/merge.tcl:212
msgid ""
"Cannot abort while amending.\n"
"\n"
"You must finish amending this commit.\n"
msgstr ""

#: lib/merge.tcl:222
msgid ""
"Abort merge?\n"
"\n"
"Aborting the current merge will cause *ALL* uncommitted changes to be lost.\n"
"\n"
"Continue with aborting the current merge?"
msgstr ""

#: lib/merge.tcl:228
msgid ""
"Reset changes?\n"
"\n"
"Resetting the changes will cause *ALL* uncommitted changes to be lost.\n"
"\n"
"Continue with resetting the current changes?"
msgstr ""

#: lib/merge.tcl:239
msgid "Aborting"
msgstr ""

#: lib/merge.tcl:266
msgid "Abort failed."
msgstr ""

#: lib/merge.tcl:268
msgid "Abort completed.  Ready."
msgstr ""

#: lib/option.tcl:82
msgid "Restore Defaults"
msgstr ""

#: lib/option.tcl:86
msgid "Save"
msgstr ""

#: lib/option.tcl:96
#, tcl-format
msgid "%s Repository"
msgstr ""

#: lib/option.tcl:97
msgid "Global (All Repositories)"
msgstr ""

#: lib/option.tcl:103
msgid "User Name"
msgstr ""

#: lib/option.tcl:104
msgid "Email Address"
msgstr ""

#: lib/option.tcl:106
msgid "Summarize Merge Commits"
msgstr ""

#: lib/option.tcl:107
msgid "Merge Verbosity"
msgstr ""

#: lib/option.tcl:108
msgid "Show Diffstat After Merge"
msgstr ""

#: lib/option.tcl:110
msgid "Trust File Modification Timestamps"
msgstr ""

#: lib/option.tcl:111
msgid "Prune Tracking Branches During Fetch"
msgstr ""

#: lib/option.tcl:112
msgid "Match Tracking Branches"
msgstr ""

#: lib/option.tcl:113
msgid "Number of Diff Context Lines"
msgstr ""

#: lib/option.tcl:114
msgid "New Branch Name Template"
msgstr ""

#: lib/option.tcl:176
msgid "Change Font"
msgstr ""

#: lib/option.tcl:180
#, tcl-format
msgid "Choose %s"
msgstr ""

#: lib/option.tcl:186
msgid "pt."
msgstr ""

#: lib/option.tcl:200
msgid "Preferences"
msgstr ""

#: lib/option.tcl:235
msgid "Failed to completely save options:"
msgstr ""

#: lib/remote_branch_delete.tcl:29 lib/remote_branch_delete.tcl:34
msgid "Delete Remote Branch"
msgstr ""

#: lib/remote_branch_delete.tcl:47
msgid "From Repository"
msgstr ""

#: lib/remote_branch_delete.tcl:50 lib/transport.tcl:123
msgid "Remote:"
msgstr ""

#: lib/remote_branch_delete.tcl:66 lib/transport.tcl:138
msgid "Arbitrary URL:"
msgstr ""

#: lib/remote_branch_delete.tcl:84
msgid "Branches"
msgstr ""

#: lib/remote_branch_delete.tcl:109
msgid "Delete Only If"
msgstr ""

#: lib/remote_branch_delete.tcl:111
msgid "Merged Into:"
msgstr ""

#: lib/remote_branch_delete.tcl:119
msgid "Always (Do not perform merge checks)"
msgstr ""

#: lib/remote_branch_delete.tcl:152
msgid "A branch is required for 'Merged Into'."
msgstr ""

#: lib/remote_branch_delete.tcl:184
#, tcl-format
msgid ""
"The following branches are not completely merged into %s:\n"
"\n"
" - %s"
msgstr ""

#: lib/remote_branch_delete.tcl:189
#, tcl-format
msgid ""
"One or more of the merge tests failed because you have not fetched the "
"necessary commits.  Try fetching from %s first."
msgstr ""

#: lib/remote_branch_delete.tcl:207
msgid "Please select one or more branches to delete."
msgstr ""

#: lib/remote_branch_delete.tcl:216
msgid ""
"Recovering deleted branches is difficult.\n"
"\n"
"Delete the selected branches?"
msgstr ""

#: lib/remote_branch_delete.tcl:226
#, tcl-format
msgid "Deleting branches from %s"
msgstr ""

#: lib/remote_branch_delete.tcl:286
msgid "No repository selected."
msgstr ""

#: lib/remote_branch_delete.tcl:291
#, tcl-format
msgid "Scanning %s..."
msgstr ""

#: lib/remote.tcl:165
msgid "Prune from"
msgstr ""

#: lib/remote.tcl:170
msgid "Fetch from"
msgstr ""

#: lib/remote.tcl:213
msgid "Push to"
msgstr ""

#: lib/shortcut.tcl:20 lib/shortcut.tcl:61
msgid "Cannot write shortcut:"
msgstr ""

#: lib/shortcut.tcl:136
msgid "Cannot write icon:"
msgstr ""

#: lib/status_bar.tcl:83
#, tcl-format
msgid "%s ... %*i of %*i %s (%3i%%)"
msgstr ""

#: lib/transport.tcl:6
#, tcl-format
msgid "fetch %s"
msgstr ""

#: lib/transport.tcl:7
#, tcl-format
msgid "Fetching new changes from %s"
msgstr ""

#: lib/transport.tcl:18
#, tcl-format
msgid "remote prune %s"
msgstr ""

#: lib/transport.tcl:19
#, tcl-format
msgid "Pruning tracking branches deleted from %s"
msgstr ""

#: lib/transport.tcl:25 lib/transport.tcl:71
#, tcl-format
msgid "push %s"
msgstr ""

#: lib/transport.tcl:26
#, tcl-format
msgid "Pushing changes to %s"
msgstr ""

#: lib/transport.tcl:72
#, tcl-format
msgid "Pushing %s %s to %s"
msgstr ""

#: lib/transport.tcl:89
msgid "Push Branches"
msgstr ""

#: lib/transport.tcl:103
msgid "Source Branches"
msgstr ""

#: lib/transport.tcl:120
msgid "Destination Repository"
msgstr ""

#: lib/transport.tcl:158
msgid "Transfer Options"
msgstr ""

#: lib/transport.tcl:160
msgid "Force overwrite existing branch (may discard changes)"
msgstr ""

#: lib/transport.tcl:164
msgid "Use thin pack (for slow network connections)"
msgstr ""

#: lib/transport.tcl:168
msgid "Include tags"
msgstr ""
