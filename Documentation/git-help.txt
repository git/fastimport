git-help(1)
===========

NAME
----
git-help - display help information about git

SYNOPSIS
--------
'git help' [-a|--all|-i|--info|-m|--man|-w|--web] [COMMAND]

DESCRIPTION
-----------

With no options and no COMMAND given, the synopsis of the 'git'
command and a list of the most commonly used git commands are printed
on the standard output.

If the option '--all' or '-a' is given, then all available commands are
printed on the standard output.

If a git command is named, a manual page for that command is brought
up. The 'man' program is used by default for this purpose, but this
can be overridden by other options or configuration variables.

Note that 'git --help ...' is identical as 'git help ...' because the
former is internally converted into the latter.

OPTIONS
-------
-a|--all::
	Prints all the available commands on the standard output. This
	option supersedes any other option.

-i|--info::
	Use the 'info' program to display the manual page, instead of
	the 'man' program that is used by default.

-m|--man::
	Use the 'man' program to display the manual page. This may be
	used to override a value set in the 'help.format'
	configuration variable.

-w|--web::
	Use a web browser to display the HTML manual page, instead of
	the 'man' program that is used by default.
+
The web browser can be specified using the configuration variable
'help.browser', or 'web.browser' if the former is not set. If none of
these config variables is set, the 'git-help--browse' helper script
(called by 'git-help') will pick a suitable default.
+
You can explicitly provide a full path to your preferred browser by
setting the configuration variable 'browser.<tool>.path'. For example,
you can configure the absolute path to firefox by setting
'browser.firefox.path'. Otherwise, 'git-help--browse' assumes the tool
is available in PATH.
+
Note that the script tries, as much as possible, to display the HTML
page in a new tab on an already opened browser.

CONFIGURATION VARIABLES
-----------------------

If no command line option is passed, the 'help.format' configuration
variable will be checked. The following values are supported for this
variable; they make 'git-help' behave as their corresponding command
line option:

* "man" corresponds to '-m|--man',
* "info" corresponds to '-i|--info',
* "web" or "html" correspond to '-w|--web',

The 'help.browser', 'web.browser' and 'browser.<tool>.path' will also
be checked if the 'web' format is chosen (either by command line
option or configuration variable). See '-w|--web' in the OPTIONS
section above.

Note that these configuration variables should probably be set using
the '--global' flag, for example like this:

------------------------------------------------
$ git config --global help.format web
$ git config --global web.browser firefox
------------------------------------------------

as they are probably more user specific than repository specific.
See linkgit:git-config[1] for more information about this.

Author
------
Written by Junio C Hamano <gitster@pobox.com> and the git-list
<git@vger.kernel.org>.

Documentation
-------------
Initial documentation was part of the linkgit:git[7] man page.
Christian Couder <chriscool@tuxfamily.org> extracted and rewrote it a
little. Maintenance is done by the git-list <git@vger.kernel.org>.

GIT
---
Part of the linkgit:git[7] suite
