git-pull(1)
===========

NAME
----
git-pull - Fetch from and merge with another repository or a local branch


SYNOPSIS
--------
'git-pull' <options> <repository> <refspec>...


DESCRIPTION
-----------
Runs `git-fetch` with the given parameters, and calls `git-merge`
to merge the retrieved head(s) into the current branch.

Note that you can use `.` (current directory) as the
<repository> to pull from the local repository -- this is useful
when merging local branches into the current branch.


OPTIONS
-------
include::merge-options.txt[]

include::fetch-options.txt[]

include::pull-fetch-param.txt[]

include::urls-remotes.txt[]

include::merge-strategies.txt[]

\--rebase::
	Instead of a merge, perform a rebase after fetching.
	*NOTE:* This is a potentially _dangerous_ mode of operation.
	It rewrites history, which does not bode well when you
	published that history already.  Do *not* use this option
	unless you have read linkgit:git-rebase[1] carefully.

\--no-rebase::
	Override earlier \--rebase.

DEFAULT BEHAVIOUR
-----------------

Often people use `git pull` without giving any parameter.
Traditionally, this has been equivalent to saying `git pull
origin`.  However, when configuration `branch.<name>.remote` is
present while on branch `<name>`, that value is used instead of
`origin`.

In order to determine what URL to use to fetch from, the value
of the configuration `remote.<origin>.url` is consulted
and if there is not any such variable, the value on `URL: ` line
in `$GIT_DIR/remotes/<origin>` file is used.

In order to determine what remote branches to fetch (and
optionally store in the tracking branches) when the command is
run without any refspec parameters on the command line, values
of the configuration variable `remote.<origin>.fetch` are
consulted, and if there aren't any, `$GIT_DIR/remotes/<origin>`
file is consulted and its `Pull: ` lines are used.
In addition to the refspec formats described in the OPTIONS
section, you can have a globbing refspec that looks like this:

------------
refs/heads/*:refs/remotes/origin/*
------------

A globbing refspec must have a non-empty RHS (i.e. must store
what were fetched in tracking branches), and its LHS and RHS
must end with `/*`.  The above specifies that all remote
branches are tracked using tracking branches in
`refs/remotes/origin/` hierarchy under the same name.

The rule to determine which remote branch to merge after
fetching is a bit involved, in order not to break backward
compatibility.

If explicit refspecs were given on the command
line of `git pull`, they are all merged.

When no refspec was given on the command line, then `git pull`
uses the refspec from the configuration or
`$GIT_DIR/remotes/<origin>`.  In such cases, the following
rules apply:

. If `branch.<name>.merge` configuration for the current
  branch `<name>` exists, that is the name of the branch at the
  remote site that is merged.

. If the refspec is a globbing one, nothing is merged.

. Otherwise the remote branch of the first refspec is merged.


EXAMPLES
--------

git pull, git pull origin::
	Update the remote-tracking branches for the repository
	you cloned from, then merge one of them into your
	current branch.  Normally the branch merged in is
	the HEAD of the remote repository, but the choice is
	determined by the branch.<name>.remote and
	branch.<name>.merge options; see linkgit:git-config[1]
	for details.

git pull origin next::
	Merge into the current branch the remote branch `next`;
	leaves a copy of `next` temporarily in FETCH_HEAD, but
	does not update any remote-tracking branches.

git pull . fixes enhancements::
	Bundle local branch `fixes` and `enhancements` on top of
	the current branch, making an Octopus merge.  This `git pull .`
	syntax is equivalent to `git merge`.

git pull -s ours . obsolete::
	Merge local branch `obsolete` into the current branch,
	using `ours` merge strategy.

git pull --no-commit . maint::
	Merge local branch `maint` into the current branch, but
	do not make a commit automatically.  This can be used
	when you want to include further changes to the merge,
	or want to write your own merge commit message.
+
You should refrain from abusing this option to sneak substantial
changes into a merge commit.  Small fixups like bumping
release/version name would be acceptable.

Command line pull of multiple branches from one repository::
+
------------------------------------------------
$ git checkout master
$ git fetch origin +pu:pu maint:tmp
$ git pull . tmp
------------------------------------------------
+
This updates (or creates, as necessary) branches `pu` and `tmp`
in the local repository by fetching from the branches
(respectively) `pu` and `maint` from the remote repository.
+
The `pu` branch will be updated even if it is does not
fast-forward; the others will not be.
+
The final command then merges the newly fetched `tmp` into master.


If you tried a pull which resulted in a complex conflicts and
would want to start over, you can recover with
linkgit:git-reset[1].


SEE ALSO
--------
linkgit:git-fetch[1], linkgit:git-merge[1], linkgit:git-config[1]


Author
------
Written by Linus Torvalds <torvalds@osdl.org>
and Junio C Hamano <junkio@cox.net>

Documentation
--------------
Documentation by Jon Loeliger,
David Greaves,
Junio C Hamano and the git-list <git@vger.kernel.org>.

GIT
---
Part of the linkgit:git[7] suite
